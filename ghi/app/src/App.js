import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import MainPage from "./MainPage";
import HatList from "./HatList";
import HatForm from "./HatForm";
import { useState, useEffect } from "react";


function App() {

  const [hats, setHats] = useState([])

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }
  }

  useEffect(() => {
    getHats();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />

        <Route path="hats">
          <Route index element={<HatList hats={hats} getHats={getHats} />} />
          <Route path="new" element={<HatForm getHats={getHats}/>} />
        </Route>

      </Routes>
    </BrowserRouter>
  );
}

export default App;
