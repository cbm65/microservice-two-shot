import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";


function HatList({ hats, getHats}) {


  const deleteHat = async (id) => {
    fetch(`http://localhost:8090/api/hats/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getHats()
    })
  }

  if (hats === undefined) {
    return null
  }


  return (
  <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{ hat.name }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td><img src={hat.picture_url} height="50" width="50"></img></td>
              <td>{ hat.location }</td>
              <td>
                  <button type="button" value={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default HatList;
