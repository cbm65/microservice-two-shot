import React, {useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const MainPage = (props) =>  {


  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <h1 className="display-5 fw-bold">Wadrobify!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The only resource you'll ever need to keep track of all your hats and shoes!
          </p>
          <div className="button">

            <Link to="/shoes/" type="button" className="btn btn-primary btn-lg btn-block">Shoes</Link>
          </div>
          <div className="button">

            <Link to="/hats/" type="button" className="btn btn-secondary btn-lg btn-block">Hats</Link>
          </div>
        </div>
      </div>

    </>
  );
}

export default MainPage;
